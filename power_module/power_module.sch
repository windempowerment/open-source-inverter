EESchema Schematic File Version 4
LIBS:Wind_module-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4700 3200 3150 1600
U 5B8E5356
F0 "POWER BLOCK" 50
F1 "power_block.sch" 50
F2 "5V_IN" I L 4700 3700 50 
F3 "Vsa" I L 4700 3800 50 
F4 "SaD" I L 4700 3900 50 
F5 "Sa" I L 4700 4000 50 
F6 "Vsb" I L 4700 4100 50 
F7 "SbD" I L 4700 4200 50 
F8 "Sb" I L 4700 4300 50 
F9 "Vsc" I L 4700 4400 50 
F10 "ScD" I L 4700 4500 50 
F11 "Sc" I L 4700 4600 50 
F12 "I_R" O R 7850 3300 50 
F13 "I_S" O R 7850 3400 50 
F14 "I_T" O R 7850 3500 50 
F15 "I_OUT" O R 7850 3600 50 
F16 "N" O L 4700 3500 50 
F17 "V_OUT_MES" O R 7850 3700 50 
F18 "V_HIGH" O L 4700 3600 50 
F19 "T_COND" O R 7850 3800 50 
F20 "S_COND" O R 7850 3900 50 
F21 "R_COND" O R 7850 4000 50 
$EndSheet
$Sheet
S 4700 1200 3150 1600
U 5B8E53AF
F0 "MEASUREMENT BLOCK" 50
F1 "measurement_block.sch" 50
F2 "5V_IN" I L 4700 1850 50 
F3 "5V_N" I L 4700 1350 50 
F4 "N" I L 4700 2050 50 
F5 "I_R" I R 7850 2750 50 
F6 "I_S" I R 7850 2650 50 
F7 "I_T" I R 7850 2550 50 
F8 "I_OUT" I R 7850 2450 50 
F9 "V_OUT_MES" I R 7850 2350 50 
F10 "CLK" I R 7850 1250 50 
F11 "MOSI" I R 7850 1550 50 
F12 "SS" I R 7850 1450 50 
F13 "SS1" I R 7850 1350 50 
F14 "MISO" O R 7850 1650 50 
F15 "R_COND" I R 7850 2050 50 
F16 "S_COND" I R 7850 2150 50 
F17 "T_COND" I R 7850 2250 50 
F18 "VCC_3V3" I R 7850 1900 50 
$EndSheet
$Sheet
S 4750 5250 1650 1550
U 5B8E5412
F0 "DRIVER BLOCK" 50
F1 "driver_block.sch" 50
F2 "SaD" O L 4750 6150 50 
F3 "Sa" O L 4750 6050 50 
F4 "SbD" O L 4750 5850 50 
F5 "Sb" O L 4750 5750 50 
F6 "ScD" O L 4750 5550 50 
F7 "Sc" O L 4750 5450 50 
F8 "15V" I L 4750 6600 50 
F9 "5V" I L 4750 6700 50 
F10 "PWM1H" I R 6400 5550 50 
F11 "PWM1L" I R 6400 5650 50 
F12 "PWM2H" I R 6400 5750 50 
F13 "PWM2L" I R 6400 5850 50 
F14 "PWM3H" I R 6400 5950 50 
F15 "PWM3L" I R 6400 6050 50 
F16 "5V_IN" I L 4750 6500 50 
F17 "Vsa" O L 4750 6250 50 
F18 "Vsb" O L 4750 5950 50 
F19 "Vsc" O L 4750 5650 50 
$EndSheet
Wire Wire Line
	8250 4700 8250 6050
Wire Wire Line
	8250 6050 6400 6050
Wire Wire Line
	6400 5950 8200 5950
Wire Wire Line
	8200 5950 8200 4600
Wire Wire Line
	8150 4500 8150 5850
Wire Wire Line
	8150 5850 6400 5850
Wire Wire Line
	6400 5750 8100 5750
Wire Wire Line
	8100 5750 8100 4400
Wire Wire Line
	8050 4300 8050 5650
Wire Wire Line
	8050 5650 6400 5650
Wire Wire Line
	6400 5550 8000 5550
Wire Wire Line
	8000 5550 8000 4200
Wire Wire Line
	4750 5450 4500 5450
Wire Wire Line
	4500 5450 4500 4600
Wire Wire Line
	4500 4600 4700 4600
Wire Wire Line
	4750 5550 4450 5550
Wire Wire Line
	4450 5550 4450 4500
Wire Wire Line
	4450 4500 4700 4500
Wire Wire Line
	4750 5650 4400 5650
Wire Wire Line
	4400 5650 4400 4400
Wire Wire Line
	4400 4400 4700 4400
Wire Wire Line
	4750 5750 4350 5750
Wire Wire Line
	4350 5750 4350 4300
Wire Wire Line
	4350 4300 4700 4300
Wire Wire Line
	4750 5850 4300 5850
Wire Wire Line
	4300 5850 4300 4200
Wire Wire Line
	4300 4200 4700 4200
Wire Wire Line
	4750 5950 4250 5950
Wire Wire Line
	4250 5950 4250 4100
Wire Wire Line
	4250 4100 4700 4100
Wire Wire Line
	4750 6050 4200 6050
Wire Wire Line
	4200 6050 4200 4000
Wire Wire Line
	4200 4000 4700 4000
Wire Wire Line
	4750 6150 4150 6150
Wire Wire Line
	4150 6150 4150 3900
Wire Wire Line
	4150 3900 4700 3900
Wire Wire Line
	4750 6250 4100 6250
Wire Wire Line
	4100 6250 4100 3800
Wire Wire Line
	4100 3800 4700 3800
Wire Wire Line
	7850 3300 7900 3300
Wire Wire Line
	7900 3300 7900 2750
Wire Wire Line
	7900 2750 7850 2750
Wire Wire Line
	7850 3400 7950 3400
Wire Wire Line
	7950 3400 7950 2650
Wire Wire Line
	7950 2650 7850 2650
Wire Wire Line
	7850 3500 8000 3500
Wire Wire Line
	8000 3500 8000 2550
Wire Wire Line
	8000 2550 7850 2550
Wire Wire Line
	7850 3600 8050 3600
Wire Wire Line
	8050 3600 8050 2450
Wire Wire Line
	8050 2450 7850 2450
Wire Wire Line
	7850 3700 8100 3700
Wire Wire Line
	8100 3700 8100 2350
Wire Wire Line
	8100 2350 7850 2350
Wire Wire Line
	7850 3800 8150 3800
Wire Wire Line
	8150 3800 8150 2250
Wire Wire Line
	8150 2250 7850 2250
Wire Wire Line
	7850 3900 8200 3900
Wire Wire Line
	8200 3900 8200 2150
Wire Wire Line
	8200 2150 7850 2150
Wire Wire Line
	7850 4000 8250 4000
Wire Wire Line
	8250 4000 8250 2050
Wire Wire Line
	8250 2050 7850 2050
Wire Wire Line
	3450 4500 3900 4500
Wire Wire Line
	4750 6600 3900 6600
Wire Wire Line
	3900 4500 3900 6600
Wire Wire Line
	4750 6700 4070 6700
Wire Wire Line
	3700 4600 3450 4600
Wire Wire Line
	4070 6700 4070 7000
Wire Wire Line
	6700 7000 6700 6200
Wire Wire Line
	6700 6200 10300 6200
Wire Wire Line
	10300 6200 10300 4650
Wire Wire Line
	10300 4650 10200 4650
Wire Wire Line
	4750 6500 4000 6500
Wire Wire Line
	3450 3300 3700 3300
Wire Wire Line
	3700 3300 3700 1350
Wire Wire Line
	3700 1350 4700 1350
Wire Wire Line
	8000 4200 8350 4200
Wire Wire Line
	8050 4300 8350 4300
Wire Wire Line
	8100 4400 8350 4400
Wire Wire Line
	8150 4500 8350 4500
Wire Wire Line
	8200 4600 8350 4600
Wire Wire Line
	8250 4700 8350 4700
$Sheet
S 2050 3200 1400 1550
U 5B8E5441
F0 "FEEDER BLOCK" 50
F1 "feeder_block.sch" 50
F2 "5V" O R 3450 4600 50 
F3 "5V_N" O R 3450 3300 50 
F4 "5V_IN" O R 3450 3700 50 
F5 "15V" O R 3450 4500 50 
F6 "N" I R 3450 3500 50 
F7 "V_HIGH" I R 3450 3600 50 
$EndSheet
$Sheet
S 8350 3200 1850 1600
U 5B8E53EB
F0 "COMMAND BLOCK" 50
F1 "command_block.sch" 50
F2 "PWM1L" O L 8350 4300 50 
F3 "PWM1H" O L 8350 4200 50 
F4 "PWM2L" O L 8350 4500 50 
F5 "PWM2H" O L 8350 4400 50 
F6 "PWM3L" O L 8350 4700 50 
F7 "PWM3H" O L 8350 4600 50 
F8 "5V" I R 10200 4650 50 
F9 "MISO" I R 10200 3400 50 
F10 "MOSI" O R 10200 3500 50 
F11 "SS" O R 10200 3600 50 
F12 "SS1" O R 10200 3700 50 
F13 "CLK" O R 10200 3800 50 
F14 "VCC_3V3" O R 10200 3300 50 
$EndSheet
Wire Wire Line
	4250 2050 4700 2050
Wire Wire Line
	4250 2050 4250 3500
Wire Wire Line
	4000 1850 4700 1850
Wire Wire Line
	4000 1850 4000 3700
Wire Wire Line
	3450 3700 4000 3700
Connection ~ 4000 3700
Wire Wire Line
	4000 3700 4000 6500
Wire Wire Line
	4000 3700 4700 3700
Wire Wire Line
	3450 3500 4250 3500
Connection ~ 4250 3500
Wire Wire Line
	4250 3500 4700 3500
Wire Wire Line
	4700 3600 3450 3600
Wire Wire Line
	10750 1250 10750 3800
Wire Wire Line
	7850 1250 10750 1250
Wire Wire Line
	10200 3800 10750 3800
Wire Wire Line
	10700 3700 10700 1350
Wire Wire Line
	7850 1350 10700 1350
Wire Wire Line
	10200 3700 10700 3700
Wire Wire Line
	10650 1450 10650 3600
Wire Wire Line
	7850 1450 10650 1450
Wire Wire Line
	10200 3600 10650 3600
Wire Wire Line
	10600 3500 10600 1550
Wire Wire Line
	7850 1550 10600 1550
Wire Wire Line
	10200 3500 10600 3500
Wire Wire Line
	10200 3400 10550 3400
Wire Wire Line
	10200 3300 10400 3300
Wire Wire Line
	10400 3300 10400 1900
Wire Wire Line
	10550 1650 7850 1650
Wire Wire Line
	10550 1650 10550 3400
Wire Wire Line
	7850 1900 10400 1900
Wire Wire Line
	3700 4600 3700 6700
Text Notes 5300 2200 0    197  ~ 39
MEASUREMENT \n    BLOCK
Text Notes 5800 4250 0    197  ~ 39
POWER \nBLOCK
Text Notes 8800 4250 0    197  ~ 39
COMM. \nBLOCK
Text Notes 5000 6250 0    197  ~ 39
DRIVER \nBLOCK
Text Notes 2150 4300 0    197  ~ 39
FEEDER \nBLOCK
Text Notes 540  800  0    197  ~ 39
OWNTECH POWER BOARD
Wire Notes Line
	760  930  4220 930 
Wire Notes Line
	4220 930  4220 550 
Text Notes 560  7670 0    47   ~ 0
POWER BLOCK - Power Electronics components, Power Connectors and measuring sensors\nMEASUREMENT BLOCK - Operational Amplifiers and data isolation \nMICROCONTROLLER BLOCK - 36 pin connector for the slave board\nDRIVER BLOCK - Digital Isolator, MOSFET Driver\nFEEDER BLOCK - Input up to 60V, Output +15V, +5V and isolated +5V \n 
Text Notes 1780 7050 0    47   ~ 0
LIST OF BLOCKS
Wire Notes Line
	1530 7110 2660 7110
Wire Notes Line
	530  6940 3950 6940
Wire Notes Line
	3950 6940 3950 7760
Connection ~ 4070 6700
Wire Wire Line
	4070 6700 3700 6700
Wire Wire Line
	4070 7000 6700 7000
Text Notes 550  1280 0    47   ~ 0
These schematics detail the blocks composing the OwnTech Wind Board.\nEach block has its own input/output variables and can be read independently.\nThis board has a connector for a dedicated slave board. \nThe microcontroller and control algorithms are hosted in the slave board. 
$EndSCHEMATC
