EESchema Schematic File Version 4
LIBS:Wind_module-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_FET:IRS2181 U12
U 1 1 5B912649
P 7360 1960
F 0 "U12" V 7406 1530 50  0000 R CNN
F 1 "IRS2181" V 7315 1530 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 7360 1510 50  0001 C CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irs2181.pdf" H 7160 1410 50  0001 C CNN
	1    7360 1960
	1    0    0    -1  
$EndComp
$Comp
L Driver_FET:IRS2181 U13
U 1 1 5B912743
P 7360 3760
F 0 "U13" H 7430 3300 50  0000 R CNN
F 1 "IRS2181" H 7520 4220 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 7360 3310 50  0001 C CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irs2181.pdf" H 7160 3210 50  0001 C CNN
	1    7360 3760
	1    0    0    -1  
$EndComp
$Comp
L Driver_FET:IRS2181 U15
U 1 1 5B9127D3
P 7360 5510
F 0 "U15" H 7430 5980 50  0000 R CNN
F 1 "IRS2181" H 7520 5040 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 7360 5060 50  0001 C CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irs2181.pdf" H 7160 4960 50  0001 C CNN
	1    7360 5510
	1    0    0    -1  
$EndComp
$Comp
L device:R R57
U 1 1 5B919408
P 6410 2060
F 0 "R57" V 6330 1990 50  0000 L CNN
F 1 "220" V 6410 1990 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6340 2060 50  0001 C CNN
F 3 "~" H 6410 2060 50  0001 C CNN
	1    6410 2060
	0    1    1    0   
$EndComp
Wire Wire Line
	6560 3760 6860 3760
Wire Wire Line
	6860 3760 6860 3860
Wire Wire Line
	6860 3860 7060 3860
Wire Wire Line
	7060 3960 6560 3960
Wire Wire Line
	6010 3610 5810 3610
Wire Wire Line
	5810 3710 6110 3710
Wire Wire Line
	6260 3760 6210 3760
Wire Wire Line
	6210 3760 6210 3810
Wire Wire Line
	6210 3810 5810 3810
Wire Wire Line
	6260 3960 6260 3910
Wire Wire Line
	6260 3910 5810 3910
Wire Wire Line
	5810 4010 6110 4010
Wire Wire Line
	5810 4110 6010 4110
Wire Wire Line
	6010 5710 6260 5710
$Comp
L power:GND #PWR0137
U 1 1 5B91AB3A
P 5860 4310
F 0 "#PWR0137" H 5860 4060 50  0001 C CNN
F 1 "GND" H 5865 4137 50  0000 C CNN
F 2 "" H 5860 4310 50  0001 C CNN
F 3 "" H 5860 4310 50  0001 C CNN
	1    5860 4310
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0138
U 1 1 5B91AB77
P 4910 4760
F 0 "#PWR0138" H 4910 4510 50  0001 C CNN
F 1 "GNDD" H 4914 4605 50  0000 C CNN
F 2 "" H 4910 4760 50  0001 C CNN
F 3 "" H 4910 4760 50  0001 C CNN
	1    4910 4760
	1    0    0    -1  
$EndComp
Wire Wire Line
	5810 4260 5860 4260
Wire Wire Line
	5010 4260 4910 4260
Wire Wire Line
	4910 4260 4910 4760
$Comp
L device:C C32
U 1 1 5B91BD06
P 5060 2960
F 0 "C32" H 4900 2870 50  0000 L CNN
F 1 "1u" H 4940 3050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5098 2810 50  0001 C CNN
F 3 "~" H 5060 2960 50  0001 C CNN
	1    5060 2960
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0139
U 1 1 5B91BD5E
P 5710 3160
F 0 "#PWR0139" H 5710 2910 50  0001 C CNN
F 1 "GND" H 5715 2987 50  0000 C CNN
F 2 "" H 5710 3160 50  0001 C CNN
F 3 "" H 5710 3160 50  0001 C CNN
	1    5710 3160
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0140
U 1 1 5B91BD81
P 5060 3160
F 0 "#PWR0140" H 5060 2910 50  0001 C CNN
F 1 "GNDD" H 5064 3005 50  0000 C CNN
F 2 "" H 5060 3160 50  0001 C CNN
F 3 "" H 5060 3160 50  0001 C CNN
	1    5060 3160
	1    0    0    -1  
$EndComp
Wire Wire Line
	5860 3460 5810 3460
Wire Wire Line
	4910 3460 5010 3460
Text Label 5750 2580 2    50   ~ 0
5V_IN
Text Label 4810 2760 2    50   ~ 0
5V
Wire Wire Line
	4560 3310 4810 3310
Wire Wire Line
	4810 3310 4810 3610
Wire Wire Line
	4810 3610 5010 3610
Wire Wire Line
	4560 3560 4760 3560
Wire Wire Line
	4760 3560 4760 3710
Wire Wire Line
	4760 3710 5010 3710
Wire Wire Line
	4560 3810 5010 3810
Wire Wire Line
	4560 4060 4610 4060
Wire Wire Line
	4610 4060 4610 3910
Wire Wire Line
	4610 3910 5010 3910
Wire Wire Line
	5010 4010 4710 4010
Wire Wire Line
	4710 4010 4710 4310
Wire Wire Line
	4710 4310 4560 4310
Wire Wire Line
	4560 4560 4810 4560
Wire Wire Line
	4810 4560 4810 4110
Wire Wire Line
	4810 4110 5010 4110
Text Label 4010 3310 2    50   ~ 0
PWM1H
Text Label 4010 3560 2    50   ~ 0
PWM1L
Text Label 4010 3810 2    50   ~ 0
PWM2H
Text Label 4010 4060 2    50   ~ 0
PWM2L
Text Label 4010 4310 2    50   ~ 0
PWM3H
Text Label 4010 4560 2    50   ~ 0
PWM3L
Wire Wire Line
	4010 3310 4260 3310
Wire Wire Line
	4260 3560 4010 3560
Wire Wire Line
	4260 3810 4010 3810
Wire Wire Line
	4260 4060 4010 4060
Wire Wire Line
	4260 4310 4010 4310
Wire Wire Line
	4260 4560 4010 4560
$Comp
L device:C C29
U 1 1 5B931AA5
P 6860 1460
F 0 "C29" H 6940 1370 50  0000 L CNN
F 1 "100n" H 6910 1570 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6898 1310 50  0001 C CNN
F 3 "~" H 6860 1460 50  0001 C CNN
	1    6860 1460
	-1   0    0    1   
$EndComp
Wire Wire Line
	7010 1660 7060 1660
Text Label 6660 1260 2    50   ~ 0
15V
$Comp
L power:GND #PWR0141
U 1 1 5B934F3E
P 6860 1660
F 0 "#PWR0141" H 6860 1410 50  0001 C CNN
F 1 "GND" H 6865 1487 50  0000 C CNN
F 2 "" H 6860 1660 50  0001 C CNN
F 3 "" H 6860 1660 50  0001 C CNN
	1    6860 1660
	1    0    0    -1  
$EndComp
Wire Wire Line
	7010 3460 7060 3460
Text Label 6660 3060 2    50   ~ 0
15V
$Comp
L power:GND #PWR0142
U 1 1 5B936BE6
P 6860 3460
F 0 "#PWR0142" H 6860 3210 50  0001 C CNN
F 1 "GND" H 6865 3287 50  0000 C CNN
F 2 "" H 6860 3460 50  0001 C CNN
F 3 "" H 6860 3460 50  0001 C CNN
	1    6860 3460
	1    0    0    -1  
$EndComp
Wire Wire Line
	7010 5210 7060 5210
Text Label 6660 4810 2    50   ~ 0
15V
$Comp
L power:GND #PWR0143
U 1 1 5B938B31
P 6860 5210
F 0 "#PWR0143" H 6860 4960 50  0001 C CNN
F 1 "GND" H 6865 5037 50  0000 C CNN
F 2 "" H 6860 5210 50  0001 C CNN
F 3 "" H 6860 5210 50  0001 C CNN
	1    6860 5210
	1    0    0    -1  
$EndComp
Wire Wire Line
	7060 5710 6560 5710
Wire Wire Line
	6560 5460 6660 5460
Wire Wire Line
	6660 5460 6660 5610
Wire Wire Line
	7060 5610 6660 5610
Wire Wire Line
	6010 4110 6010 5710
Wire Wire Line
	6110 5460 6260 5460
Wire Wire Line
	6110 4010 6110 5460
$Comp
L power:GND #PWR0144
U 1 1 5B942D20
P 7010 4110
F 0 "#PWR0144" H 7010 3860 50  0001 C CNN
F 1 "GND" H 7015 3937 50  0000 C CNN
F 2 "" H 7010 4110 50  0001 C CNN
F 3 "" H 7010 4110 50  0001 C CNN
	1    7010 4110
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0145
U 1 1 5B942D55
P 7010 5860
F 0 "#PWR0145" H 7010 5610 50  0001 C CNN
F 1 "GND" H 7015 5687 50  0000 C CNN
F 2 "" H 7010 5860 50  0001 C CNN
F 3 "" H 7010 5860 50  0001 C CNN
	1    7010 5860
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0146
U 1 1 5B943276
P 7010 2310
F 0 "#PWR0146" H 7010 2060 50  0001 C CNN
F 1 "GND" H 7015 2137 50  0000 C CNN
F 2 "" H 7010 2310 50  0001 C CNN
F 3 "" H 7010 2310 50  0001 C CNN
	1    7010 2310
	1    0    0    -1  
$EndComp
Wire Wire Line
	6260 2060 6010 2060
Wire Wire Line
	6260 2310 6110 2310
Wire Wire Line
	7060 2060 6560 2060
Wire Wire Line
	6010 2060 6010 3610
Wire Wire Line
	6110 2310 6110 3710
Wire Wire Line
	6560 2310 6610 2310
Wire Wire Line
	6610 2310 6610 2160
Wire Wire Line
	7060 2160 6610 2160
$Comp
L device:R R56
U 1 1 5B951FA6
P 7960 1910
F 0 "R56" V 7870 1850 50  0000 L CNN
F 1 "10" V 7960 1860 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7890 1910 50  0001 C CNN
F 3 "~" H 7960 1910 50  0001 C CNN
	1    7960 1910
	0    1    1    0   
$EndComp
Wire Wire Line
	7660 5610 7710 5610
Wire Wire Line
	7710 5610 7710 5460
Wire Wire Line
	7710 5460 7760 5460
Wire Wire Line
	7660 5810 7710 5810
Wire Wire Line
	7710 5810 7710 5910
Wire Wire Line
	7710 5910 7760 5910
Wire Wire Line
	7660 4060 7710 4060
Wire Wire Line
	7710 4060 7710 4160
Wire Wire Line
	7710 4160 7810 4160
Wire Wire Line
	7660 3860 7710 3860
Wire Wire Line
	7710 3860 7710 3760
Wire Wire Line
	7710 3760 7810 3760
Wire Wire Line
	7810 2460 7710 2460
Wire Wire Line
	7710 2460 7710 2260
Wire Wire Line
	7710 2260 7660 2260
Wire Wire Line
	7660 2060 7710 2060
Wire Wire Line
	7710 2060 7710 1910
Wire Wire Line
	7710 1910 7810 1910
$Comp
L device:R R58
U 1 1 5B965F20
P 6410 2310
F 0 "R58" V 6330 2240 50  0000 L CNN
F 1 "220" V 6410 2240 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6340 2310 50  0001 C CNN
F 3 "~" H 6410 2310 50  0001 C CNN
	1    6410 2310
	0    1    1    0   
$EndComp
$Comp
L device:R R63
U 1 1 5B965F5C
P 6410 3760
F 0 "R63" V 6330 3680 50  0000 L CNN
F 1 "220" V 6410 3690 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6340 3760 50  0001 C CNN
F 3 "~" H 6410 3760 50  0001 C CNN
	1    6410 3760
	0    1    1    0   
$EndComp
$Comp
L device:R R65
U 1 1 5B965FC6
P 6410 3960
F 0 "R65" V 6330 3890 50  0000 L CNN
F 1 "220" V 6410 3890 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6340 3960 50  0001 C CNN
F 3 "~" H 6410 3960 50  0001 C CNN
	1    6410 3960
	0    1    1    0   
$EndComp
$Comp
L device:R R71
U 1 1 5B966000
P 6410 5460
F 0 "R71" V 6330 5400 50  0000 L CNN
F 1 "220" V 6410 5400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6340 5460 50  0001 C CNN
F 3 "~" H 6410 5460 50  0001 C CNN
	1    6410 5460
	0    1    1    0   
$EndComp
$Comp
L device:R R72
U 1 1 5B966076
P 6410 5710
F 0 "R72" V 6330 5640 50  0000 L CNN
F 1 "220" V 6410 5640 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6340 5710 50  0001 C CNN
F 3 "~" H 6410 5710 50  0001 C CNN
	1    6410 5710
	0    1    1    0   
$EndComp
$Comp
L device:R R60
U 1 1 5B9660B6
P 4410 3310
F 0 "R60" V 4330 3250 50  0000 L CNN
F 1 "220" V 4410 3250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4340 3310 50  0001 C CNN
F 3 "~" H 4410 3310 50  0001 C CNN
	1    4410 3310
	0    1    1    0   
$EndComp
$Comp
L device:R R61
U 1 1 5B9661E0
P 4410 3560
F 0 "R61" V 4320 3500 50  0000 L CNN
F 1 "220" V 4410 3490 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4340 3560 50  0001 C CNN
F 3 "~" H 4410 3560 50  0001 C CNN
	1    4410 3560
	0    1    1    0   
$EndComp
$Comp
L device:R R64
U 1 1 5B966285
P 4410 3810
F 0 "R64" V 4330 3740 50  0000 L CNN
F 1 "220" V 4410 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4340 3810 50  0001 C CNN
F 3 "~" H 4410 3810 50  0001 C CNN
	1    4410 3810
	0    1    1    0   
$EndComp
$Comp
L device:R R66
U 1 1 5B966397
P 4410 4060
F 0 "R66" V 4330 4000 50  0000 L CNN
F 1 "220" V 4410 3990 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4340 4060 50  0001 C CNN
F 3 "~" H 4410 4060 50  0001 C CNN
	1    4410 4060
	0    1    1    0   
$EndComp
$Comp
L device:R R68
U 1 1 5B9663E3
P 4410 4310
F 0 "R68" V 4320 4230 50  0000 L CNN
F 1 "220" V 4410 4240 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4340 4310 50  0001 C CNN
F 3 "~" H 4410 4310 50  0001 C CNN
	1    4410 4310
	0    1    1    0   
$EndComp
$Comp
L device:R R69
U 1 1 5B96642D
P 4410 4560
F 0 "R69" V 4320 4480 50  0000 L CNN
F 1 "220" V 4410 4490 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4340 4560 50  0001 C CNN
F 3 "~" H 4410 4560 50  0001 C CNN
	1    4410 4560
	0    1    1    0   
$EndComp
$Comp
L device:C C31
U 1 1 5B966583
P 5710 2960
F 0 "C31" H 5750 2870 50  0000 L CNN
F 1 "1u" H 5780 3050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5748 2810 50  0001 C CNN
F 3 "~" H 5710 2960 50  0001 C CNN
	1    5710 2960
	-1   0    0    1   
$EndComp
$Comp
L device:C C33
U 1 1 5B9667B0
P 6860 3260
F 0 "C33" H 6930 3170 50  0000 L CNN
F 1 "100n" H 6910 3370 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6898 3110 50  0001 C CNN
F 3 "~" H 6860 3260 50  0001 C CNN
	1    6860 3260
	-1   0    0    1   
$EndComp
$Comp
L device:C C35
U 1 1 5B966839
P 6860 5010
F 0 "C35" H 6960 4940 50  0000 L CNN
F 1 "100n" H 6910 5120 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6898 4860 50  0001 C CNN
F 3 "~" H 6860 5010 50  0001 C CNN
	1    6860 5010
	-1   0    0    1   
$EndComp
$Comp
L device:R R59
U 1 1 5B9669F2
P 7960 2460
F 0 "R59" V 7870 2390 50  0000 L CNN
F 1 "10" V 7960 2400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7890 2460 50  0001 C CNN
F 3 "~" H 7960 2460 50  0001 C CNN
	1    7960 2460
	0    1    1    0   
$EndComp
$Comp
L device:R R62
U 1 1 5B966B1F
P 7960 3760
F 0 "R62" V 7870 3700 50  0000 L CNN
F 1 "10" V 7960 3720 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7890 3760 50  0001 C CNN
F 3 "~" H 7960 3760 50  0001 C CNN
	1    7960 3760
	0    1    1    0   
$EndComp
$Comp
L device:R R67
U 1 1 5B966B8B
P 7960 4160
F 0 "R67" V 7870 4090 50  0000 L CNN
F 1 "10" V 7960 4110 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7890 4160 50  0001 C CNN
F 3 "~" H 7960 4160 50  0001 C CNN
	1    7960 4160
	0    1    1    0   
$EndComp
$Comp
L device:R R70
U 1 1 5B966C69
P 7910 5460
F 0 "R70" V 7830 5400 50  0000 L CNN
F 1 "10" V 7910 5410 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7840 5460 50  0001 C CNN
F 3 "~" H 7910 5460 50  0001 C CNN
	1    7910 5460
	0    1    1    0   
$EndComp
$Comp
L device:R R73
U 1 1 5B966CDB
P 7910 5910
F 0 "R73" V 7820 5850 50  0000 L CNN
F 1 "10" V 7910 5860 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7840 5910 50  0001 C CNN
F 3 "~" H 7910 5910 50  0001 C CNN
	1    7910 5910
	0    1    1    0   
$EndComp
Text Label 8210 1910 0    50   ~ 0
SaD
Text Label 8210 2460 0    50   ~ 0
Sa
Text Label 8210 3760 0    50   ~ 0
SbD
Text Label 8210 4160 0    50   ~ 0
Sb
Text Label 8160 5460 0    50   ~ 0
ScD
Text Label 8160 5910 0    50   ~ 0
Sc
Wire Wire Line
	8060 5910 8160 5910
Wire Wire Line
	8060 5460 8160 5460
Wire Wire Line
	8110 4160 8210 4160
Wire Wire Line
	8110 3760 8210 3760
Wire Wire Line
	8110 2460 8210 2460
Wire Wire Line
	8110 1910 8210 1910
$Comp
L device:D D8
U 1 1 5B97BCD4
P 7710 1460
F 0 "D8" V 7756 1381 50  0000 R CNN
F 1 "D" V 7665 1381 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 7710 1460 50  0001 C CNN
F 3 "~" H 7710 1460 50  0001 C CNN
	1    7710 1460
	0    -1   -1   0   
$EndComp
$Comp
L device:C C30
U 1 1 5B983560
P 8610 1910
F 0 "C30" H 8710 1820 50  0000 C CNN
F 1 "0.47u" H 8720 2020 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 8648 1760 50  0001 C CNN
F 3 "~" H 8610 1910 50  0001 C CNN
	1    8610 1910
	-1   0    0    1   
$EndComp
Wire Wire Line
	8610 2160 8610 2060
Wire Wire Line
	8610 1760 8610 1660
$Comp
L device:D D9
U 1 1 5B98F53F
P 7710 3260
F 0 "D9" V 7756 3181 50  0000 R CNN
F 1 "D" V 7665 3181 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 7710 3260 50  0001 C CNN
F 3 "~" H 7710 3260 50  0001 C CNN
	1    7710 3260
	0    -1   -1   0   
$EndComp
$Comp
L device:C C34
U 1 1 5B98F547
P 8610 3710
F 0 "C34" H 8730 3600 50  0000 C CNN
F 1 "0.47u" H 8740 3810 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 8648 3560 50  0001 C CNN
F 3 "~" H 8610 3710 50  0001 C CNN
	1    8610 3710
	-1   0    0    1   
$EndComp
Wire Wire Line
	8610 3960 8610 3860
Wire Wire Line
	8610 3560 8610 3460
$Comp
L device:D D10
U 1 1 5B993CA3
P 7710 5010
F 0 "D10" V 7756 4931 50  0000 R CNN
F 1 "D" V 7665 4931 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 7710 5010 50  0001 C CNN
F 3 "~" H 7710 5010 50  0001 C CNN
	1    7710 5010
	0    -1   -1   0   
$EndComp
$Comp
L device:C C36
U 1 1 5B993CAB
P 8610 5460
F 0 "C36" H 8720 5370 50  0000 C CNN
F 1 "0.47u" H 8740 5560 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 8648 5310 50  0001 C CNN
F 3 "~" H 8610 5460 50  0001 C CNN
	1    8610 5460
	-1   0    0    1   
$EndComp
Wire Wire Line
	8610 5710 8610 5610
Wire Wire Line
	8610 5310 8610 5210
Wire Wire Line
	8610 2160 7660 2160
Wire Wire Line
	8610 1660 7710 1660
Wire Wire Line
	8610 3960 7660 3960
Wire Wire Line
	8610 3460 7710 3460
Wire Wire Line
	8610 5710 7660 5710
Wire Wire Line
	8610 5210 7710 5210
Text Label 10550 1300 2    50   ~ 0
Sc
Text Label 10550 1200 2    50   ~ 0
ScD
Text Label 10550 1100 2    50   ~ 0
Sb
Text Label 10550 1000 2    50   ~ 0
SbD
Text Label 10550 900  2    50   ~ 0
Sa
Text Label 10550 800  2    50   ~ 0
SaD
Text HLabel 10950 800  2    50   Output ~ 0
SaD
Text HLabel 10950 900  2    50   Output ~ 0
Sa
Text HLabel 10950 1000 2    50   Output ~ 0
SbD
Text HLabel 10950 1100 2    50   Output ~ 0
Sb
Text HLabel 10950 1200 2    50   Output ~ 0
ScD
Text HLabel 10950 1300 2    50   Output ~ 0
Sc
Wire Wire Line
	10550 800  10950 800 
Wire Wire Line
	10950 900  10550 900 
Wire Wire Line
	10550 1000 10950 1000
Wire Wire Line
	10550 1100 10950 1100
Wire Wire Line
	10550 1200 10950 1200
Wire Wire Line
	10550 1300 10950 1300
Text Label 1250 1100 0    50   ~ 0
PWM1H
Text Label 1250 1200 0    50   ~ 0
PWM1L
Text Label 1250 1300 0    50   ~ 0
PWM2H
Text Label 1250 1400 0    50   ~ 0
PWM2L
Text Label 1250 1500 0    50   ~ 0
PWM3H
Text Label 1250 1600 0    50   ~ 0
PWM3L
Text Label 1250 800  0    50   ~ 0
15V
Text Label 1250 1000 0    50   ~ 0
5V
Text Label 1250 900  0    50   ~ 0
5V_IN
Text HLabel 950  900  0    50   Input ~ 0
5V_IN
Text HLabel 950  800  0    50   Input ~ 0
15V
Text HLabel 950  1000 0    50   Input ~ 0
5V
Text HLabel 950  1100 0    50   Input ~ 0
PWM1H
Text HLabel 950  1200 0    50   Input ~ 0
PWM1L
Text HLabel 950  1300 0    50   Input ~ 0
PWM2H
Text HLabel 950  1400 0    50   Input ~ 0
PWM2L
Text HLabel 950  1500 0    50   Input ~ 0
PWM3H
Text HLabel 950  1600 0    50   Input ~ 0
PWM3L
Wire Wire Line
	950  800  1250 800 
Wire Wire Line
	950  1100 1250 1100
Wire Wire Line
	950  1200 1250 1200
Wire Wire Line
	950  1300 1250 1300
Wire Wire Line
	950  1400 1250 1400
Wire Wire Line
	950  1500 1250 1500
Wire Wire Line
	950  1600 1250 1600
$Comp
L Wind_module_lib:SI6660 U14
U 1 1 5B8EFD58
P 5410 3860
F 0 "U14" H 5680 3280 50  0000 R CNN
F 1 "SI8660" H 5260 3280 50  0000 R CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 5410 3310 50  0001 C CIN
F 3 "https://www.silabs.com/documents/public/data-sheets/si864x-datasheet.pdf" H 5410 4260 50  0001 C CNN
	1    5410 3860
	1    0    0    -1  
$EndComp
Wire Wire Line
	8610 2160 8710 2160
Connection ~ 8610 2160
Wire Wire Line
	8610 3960 8710 3960
Connection ~ 8610 3960
Wire Wire Line
	8610 5710 8710 5710
Connection ~ 8610 5710
Text Label 8710 2160 0    50   ~ 0
Vsa
Text Label 8710 3960 0    50   ~ 0
Vsb
Text Label 8710 5710 0    50   ~ 0
Vsc
Text Label 10550 1400 2    50   ~ 0
Vsa
Text Label 10550 1500 2    50   ~ 0
Vsb
Text Label 10550 1600 2    50   ~ 0
Vsc
Wire Wire Line
	950  900  1250 900 
Wire Wire Line
	950  1000 1250 1000
Text HLabel 10950 1400 2    50   Output ~ 0
Vsa
Text HLabel 10950 1500 2    50   Output ~ 0
Vsb
Text HLabel 10950 1600 2    50   Output ~ 0
Vsc
Wire Wire Line
	10550 1400 10950 1400
Wire Wire Line
	10550 1500 10950 1500
Wire Wire Line
	10550 1600 10950 1600
Wire Notes Line
	10200 500  10200 1700
Wire Notes Line
	10200 1700 11200 1700
Wire Notes Line
	10200 700  11150 700 
Wire Notes Line
	500  1700 1600 1700
Wire Notes Line
	1600 1700 1600 500 
Wire Notes Line
	500  700  1600 700 
Text Notes 10450 600  0    50   ~ 0
BLOCK OUTPUTS
Text Notes 800  600  0    50   ~ 0
BLOCK INTPUTS
Wire Wire Line
	5060 3160 5060 3110
Wire Wire Line
	5710 3160 5710 3110
Wire Wire Line
	6860 1660 6860 1610
Wire Wire Line
	7010 2310 7010 2260
Wire Wire Line
	7010 2260 7060 2260
Wire Wire Line
	6860 3460 6860 3410
Wire Wire Line
	5860 4310 5860 4260
Wire Wire Line
	6860 5210 6860 5160
Wire Wire Line
	7010 5860 7010 5810
Wire Wire Line
	7010 5810 7060 5810
Wire Wire Line
	7010 4110 7010 4060
Wire Wire Line
	7010 4060 7060 4060
Wire Wire Line
	7710 1660 7710 1610
Connection ~ 7710 1660
Wire Wire Line
	7710 1660 7660 1660
Wire Wire Line
	6860 1310 6860 1260
Wire Wire Line
	6860 1260 7010 1260
Wire Wire Line
	7010 1260 7010 1660
Wire Wire Line
	7010 1260 7710 1260
Wire Wire Line
	7710 1260 7710 1310
Connection ~ 7010 1260
Wire Wire Line
	6860 1260 6660 1260
Connection ~ 6860 1260
Wire Wire Line
	7710 3460 7710 3410
Connection ~ 7710 3460
Wire Wire Line
	7710 3460 7660 3460
Wire Wire Line
	7710 3110 7710 3060
Wire Wire Line
	7710 3060 7010 3060
Wire Wire Line
	7010 3060 7010 3460
Wire Wire Line
	6860 3110 6860 3060
Wire Wire Line
	6860 3060 7010 3060
Connection ~ 7010 3060
Wire Wire Line
	6660 3060 6860 3060
Connection ~ 6860 3060
Wire Wire Line
	7710 5160 7710 5210
Connection ~ 7710 5210
Wire Wire Line
	7710 5210 7660 5210
Wire Wire Line
	7010 4810 7710 4810
Wire Wire Line
	7710 4810 7710 4860
Wire Wire Line
	7010 4810 7010 5210
Wire Wire Line
	6860 4860 6860 4810
Wire Wire Line
	6860 4810 7010 4810
Connection ~ 7010 4810
Wire Wire Line
	6860 4810 6660 4810
Connection ~ 6860 4810
Wire Wire Line
	5710 2810 5710 2760
Wire Wire Line
	5710 2760 5860 2760
Connection ~ 5710 2760
Wire Wire Line
	5060 2810 5060 2760
Wire Wire Line
	5060 2760 4910 2760
Connection ~ 4910 2760
Wire Wire Line
	4910 2760 4810 2760
Wire Wire Line
	4910 2760 4910 3460
Wire Wire Line
	5860 2760 5860 3460
Text Notes 5440 5300 1    47   ~ 0
ISOLATION BARRIER
Text Notes 5450 3330 1    47   ~ 0
ISOLATION BARRIER
Wire Notes Line
	4640 2840 4640 5040
Wire Notes Line
	4640 5040 2560 5040
Wire Notes Line
	2560 5040 2560 2840
Wire Notes Line
	2560 2840 4640 2840
Text Notes 2660 2980 0    47   ~ 0
PWM INPUT SIGNALS
Text Notes 2620 4940 0    47   ~ 0
These signals are the PWM input coming \nfrom the slave board through the 36-pin\nconnector or from another source \nthrough the debugging pins.
Text Notes 6570 1020 0    47   ~ 0
The power driver chosen for this converter is the \nIR2186 capable of driving a transitor up to 4A at a time.\nThis solution comprises a bootstrap which allows driving \nboth the high and low side transistors from the same IC.
Text Notes 7270 620  0    47   ~ 0
DRIVER CIRCUIT
Wire Notes Line
	6590 650  8450 650 
Text Notes 2980 2360 0    47   ~ 0
The digital isolator chosen for this \nconverter is a Si8660which\nhas a VCC of +5V and can easily go up \nto 100kHz for low power applications\n\nIt requires two decoupling capacitors C31 and C32.\nFor noisy settings, the datasheet recommends a \nseries resistance with inputs and outpus between 50 to 300
Text Notes 3390 1590 0    47   ~ 0
DIGITAL ISOLATOR
Wire Notes Line
	2980 1640 4630 1640
Text Notes 9210 2840 0    47   ~ 0
The series resistance at the output of HO and LO\n(R56, R59, R62, R67, R70 and R72)\nshould be adapted to the MOSFET being used. \nIf the MOSFET is 200Vmax, then this resistance\nshould be around 2.2Ohm. \nFor a 600V MOSFET, this resistance is 10Ohm. \n\n
Text Notes 9220 3760 0    47   ~ 0
The bootstrap capacitor should be\nnon-polarized. It should hold up \nsome charge as to kick-start the\nhigh-level MOSFET. (see datasheet)\n\nThe value chosen here was 470nF. 
Text Notes 9240 4980 0    47   ~ 0
The bootstrap diodes \n(D8, D9 and D10)\nmust have a maximum \nreverse voltage must be compatible\nwith the VOUT max voltage. \nFailure to do so will destroy the \nfeeder circuit.
Text Notes 9670 1900 0    47   ~ 0
IMPLEMENTATION COMMENTS
Wire Notes Line
	9210 1940 11070 1940
Text Notes 1750 1280 0    47   ~ 0
Based on these components\nthe system operates at 100kHz
Text Notes 1750 1040 0    47   ~ 0
This circuit connects the microcontroller \nto the power transistors. \nIt uses a digital isolator to separate the grounds from both sides.\nThe digital isolator has a non-inverter output.
Text Notes 1750 600  0    47   ~ 0
DRIVER BLOCK
Wire Notes Line
	1730 660  4140 660 
Wire Wire Line
	5630 2760 5630 2580
Wire Wire Line
	5630 2580 5750 2580
Wire Wire Line
	5630 2760 5710 2760
Wire Notes Line
	5360 2510 5360 5370
Wire Notes Line
	5360 5370 4450 5370
Wire Notes Line
	2730 2510 2730 1510
Wire Notes Line
	2730 2510 5360 2510
Wire Notes Line
	5880 5370 5460 5370
Wire Notes Line
	5460 5370 5460 1130
Wire Notes Line
	5460 1130 6260 1130
$EndSCHEMATC
