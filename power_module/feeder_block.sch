EESchema Schematic File Version 4
LIBS:Wind_module-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L device:LED D12
U 1 1 5B8F0BA7
P 1400 7180
F 0 "D12" V 1438 7063 50  0000 R CNN
F 1 "LED" V 1347 7063 50  0000 R CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1400 7180 50  0001 C CNN
F 3 "~" H 1400 7180 50  0001 C CNN
	1    1400 7180
	0    -1   -1   0   
$EndComp
$Comp
L device:LED D11
U 1 1 5B8F0C4A
P 6110 4490
F 0 "D11" V 6148 4373 50  0000 R CNN
F 1 "LED" V 6057 4373 50  0000 R CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6110 4490 50  0001 C CNN
F 3 "~" H 6110 4490 50  0001 C CNN
	1    6110 4490
	0    -1   -1   0   
$EndComp
$Comp
L device:R R75
U 1 1 5B8F0CA3
P 1400 6780
F 0 "R75" H 1470 6826 50  0000 L CNN
F 1 "220" H 1470 6735 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1330 6780 50  0001 C CNN
F 3 "~" H 1400 6780 50  0001 C CNN
	1    1400 6780
	1    0    0    -1  
$EndComp
$Comp
L device:R R74
U 1 1 5B8F0D21
P 6110 3890
F 0 "R74" H 6180 3936 50  0000 L CNN
F 1 "10k" H 6180 3845 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6040 3890 50  0001 C CNN
F 3 "~" H 6110 3890 50  0001 C CNN
	1    6110 3890
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6930 1400 7030
$Comp
L power:GND #PWR0150
U 1 1 5B8F6F8F
P 1400 7430
F 0 "#PWR0150" H 1400 7180 50  0001 C CNN
F 1 "GND" H 1405 7257 50  0000 C CNN
F 2 "" H 1400 7430 50  0001 C CNN
F 3 "" H 1400 7430 50  0001 C CNN
	1    1400 7430
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 7430 1400 7330
Text Label 1400 6530 1    50   ~ 0
5V_IN
Wire Wire Line
	1400 6630 1400 6530
$Comp
L Wind_module_lib:0505-5V U16
U 1 1 5B8F9BCD
P 9600 4970
F 0 "U16" H 9878 4685 50  0000 L CNN
F 1 "ROE_1505S" H 9878 4594 50  0000 L CNN
F 2 "Wind_module:TME_0505s" H 9600 4970 50  0001 C CNN
F 3 "" H 9600 4970 50  0001 C CNN
	1    9600 4970
	1    0    0    -1  
$EndComp
$Comp
L Wind_module_lib:0505-5V U18
U 1 1 5B8FA534
P 9590 2580
F 0 "U18" H 9868 2295 50  0000 L CNN
F 1 "ROE_1505S" H 9868 2204 50  0000 L CNN
F 2 "Wind_module:TME_0505s" H 9590 2580 50  0001 C CNN
F 3 "" H 9590 2580 50  0001 C CNN
	1    9590 2580
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0156
U 1 1 5B8FBBC3
P 9440 3180
F 0 "#PWR0156" H 9440 2930 50  0001 C CNN
F 1 "GND" H 9445 3007 50  0000 C CNN
F 2 "" H 9440 3180 50  0001 C CNN
F 3 "" H 9440 3180 50  0001 C CNN
	1    9440 3180
	1    0    0    -1  
$EndComp
Text Label 8980 1780 0    50   ~ 0
N
Text Label 8440 1680 0    50   ~ 0
5V_N
Text Label 6860 3590 0    50   ~ 0
15V
Text Label 10350 1050 2    50   ~ 0
5V
Text Label 10350 1150 2    50   ~ 0
5V_N
Text HLabel 10750 1050 2    50   Output ~ 0
5V
Text HLabel 10750 1150 2    50   Output ~ 0
5V_N
Wire Wire Line
	10350 1050 10750 1050
Wire Wire Line
	10350 1150 10750 1150
Text Label 10350 950  2    50   ~ 0
5V_IN
Text HLabel 10750 950  2    50   Output ~ 0
5V_IN
Wire Wire Line
	10350 950  10750 950 
Text Label 10350 850  2    50   ~ 0
15V
Text HLabel 10750 850  2    50   Output ~ 0
15V
Wire Wire Line
	10350 850  10750 850 
Wire Notes Line
	9600 700  11200 700 
Text Notes 10150 600  0    50   ~ 0
BLOCK OUTPUTS
Wire Wire Line
	9640 3130 9640 3380
Text Label 1100 750  0    50   ~ 0
N
Text HLabel 850  750  0    50   Input ~ 0
N
Wire Wire Line
	850  750  1100 750 
Text Notes 700  600  0    50   ~ 0
BLOCK INPUTS
$Comp
L device:CP C39
U 1 1 5B998272
P 2360 3590
F 0 "C39" H 2478 3636 50  0000 L CNN
F 1 "22u" H 2478 3545 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.9" H 2398 3440 50  0001 C CNN
F 3 "~" H 2360 3590 50  0001 C CNN
	1    2360 3590
	1    0    0    -1  
$EndComp
$Comp
L device:C C40
U 1 1 5B9983C2
P 2760 3590
F 0 "C40" H 2875 3636 50  0000 L CNN
F 1 "4.7u" H 2875 3545 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2798 3440 50  0001 C CNN
F 3 "~" H 2760 3590 50  0001 C CNN
	1    2760 3590
	1    0    0    -1  
$EndComp
$Comp
L Wind_module_lib:LMR16010 U19
U 1 1 5B99CDE9
P 3910 3590
F 0 "U19" H 3910 4218 50  0000 C CNN
F 1 "LMR16010" H 3910 4127 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3910 3590 50  0001 C CNN
F 3 "" H 3910 3590 50  0001 C CNN
	1    3910 3590
	1    0    0    -1  
$EndComp
$Comp
L device:R R78
U 1 1 5B99CEC9
P 3060 3890
F 0 "R78" H 3130 3936 50  0000 L CNN
F 1 "10k" H 3130 3845 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2990 3890 50  0001 C CNN
F 3 "~" H 3060 3890 50  0001 C CNN
	1    3060 3890
	1    0    0    -1  
$EndComp
$Comp
L device:R R79
U 1 1 5B99CF42
P 3060 4290
F 0 "R79" H 3130 4336 50  0000 L CNN
F 1 "10k" H 3130 4245 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2990 4290 50  0001 C CNN
F 3 "~" H 3060 4290 50  0001 C CNN
	1    3060 4290
	1    0    0    -1  
$EndComp
$Comp
L device:R R80
U 1 1 5B99CF82
P 3060 4690
F 0 "R80" H 3130 4736 50  0000 L CNN
F 1 "10k" H 3130 4645 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2990 4690 50  0001 C CNN
F 3 "~" H 3060 4690 50  0001 C CNN
	1    3060 4690
	1    0    0    -1  
$EndComp
Wire Wire Line
	3510 3590 3060 3590
Wire Wire Line
	3060 3590 3060 3740
Wire Wire Line
	3060 4040 3060 4140
Wire Wire Line
	3060 4440 3060 4540
Wire Wire Line
	3910 3140 3910 3090
Wire Wire Line
	3910 3090 3310 3090
Wire Wire Line
	2360 3090 2360 3440
Wire Wire Line
	2760 3440 2760 3090
Connection ~ 2760 3090
Wire Wire Line
	2760 3090 2360 3090
Wire Wire Line
	2760 3740 2760 4890
Wire Wire Line
	2760 4890 3060 4890
Wire Wire Line
	3060 4890 3060 4840
Wire Wire Line
	2360 3740 2360 4890
Wire Wire Line
	2360 4890 2760 4890
Connection ~ 2760 4890
Wire Wire Line
	3510 3490 3310 3490
Wire Wire Line
	3310 3490 3310 3090
Connection ~ 3310 3090
Wire Wire Line
	3310 3090 2760 3090
Wire Wire Line
	3060 4890 3910 4890
Wire Wire Line
	3910 4890 3910 4040
Connection ~ 3060 4890
$Comp
L device:C C41
U 1 1 5B9A96F6
P 5060 3490
F 0 "C41" H 5175 3536 50  0000 L CNN
F 1 "0.1u" H 5175 3445 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5098 3340 50  0001 C CNN
F 3 "~" H 5060 3490 50  0001 C CNN
	1    5060 3490
	0    -1   -1   0   
$EndComp
$Comp
L device:D_Schottky D13
U 1 1 5B9AE203
P 4660 3790
F 0 "D13" V 4614 3869 50  0000 L CNN
F 1 "D_Schottky" V 4705 3869 50  0000 L CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4660 3790 50  0001 C CNN
F 3 "~" H 4660 3790 50  0001 C CNN
	1    4660 3790
	0    1    1    0   
$EndComp
Wire Wire Line
	4310 3590 4660 3590
Wire Wire Line
	4660 3590 4660 3640
Wire Wire Line
	4310 3490 4910 3490
Wire Wire Line
	4660 3590 5210 3590
Wire Wire Line
	5210 3590 5210 3490
Connection ~ 4660 3590
$Comp
L device:L L4
U 1 1 5B9B31BB
P 5410 3590
F 0 "L4" V 5600 3590 50  0000 C CNN
F 1 "33u" V 5509 3590 50  0000 C CNN
F 2 "Wind_module:inductance_murata_1200LRS" H 5410 3590 50  0001 C CNN
F 3 "~" H 5410 3590 50  0001 C CNN
	1    5410 3590
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5210 3590 5260 3590
Connection ~ 5210 3590
$Comp
L device:R R82
U 1 1 5B9B4DFB
P 5610 3790
F 0 "R82" H 5680 3836 50  0000 L CNN
F 1 "150k" H 5680 3745 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5540 3790 50  0001 C CNN
F 3 "~" H 5610 3790 50  0001 C CNN
	1    5610 3790
	1    0    0    -1  
$EndComp
Wire Wire Line
	5560 3590 5610 3590
Wire Wire Line
	5610 3590 5610 3640
Wire Wire Line
	4310 3690 4360 3690
Wire Wire Line
	4360 3690 4360 4140
Wire Wire Line
	4360 4140 4510 4140
Wire Wire Line
	5610 4140 5610 3940
Connection ~ 3910 4890
Connection ~ 5610 3590
$Comp
L device:CP C42
U 1 1 5B9C1B42
P 6710 4140
F 0 "C42" H 6828 4186 50  0000 L CNN
F 1 "22u" H 6828 4095 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.9" H 6748 3990 50  0001 C CNN
F 3 "~" H 6710 4140 50  0001 C CNN
	1    6710 4140
	1    0    0    -1  
$EndComp
Wire Wire Line
	6110 3590 6110 3740
Wire Wire Line
	5610 3590 6110 3590
Wire Wire Line
	6110 4040 6110 4340
Wire Wire Line
	6110 4890 6110 4640
Wire Wire Line
	6110 3590 6710 3590
Wire Wire Line
	6710 3590 6710 3990
Connection ~ 6110 3590
Wire Wire Line
	6710 4290 6710 4890
Wire Wire Line
	6710 4890 6110 4890
Connection ~ 6110 4890
$Comp
L power:GND #PWR0152
U 1 1 5B9CE417
P 4660 3990
F 0 "#PWR0152" H 4660 3740 50  0001 C CNN
F 1 "GND" H 4665 3817 50  0000 C CNN
F 2 "" H 4660 3990 50  0001 C CNN
F 3 "" H 4660 3990 50  0001 C CNN
	1    4660 3990
	1    0    0    -1  
$EndComp
Wire Wire Line
	4660 3990 4660 3940
Wire Wire Line
	3910 4890 3910 4940
$Comp
L power:GND #PWR0153
U 1 1 5B9D3D2E
P 3910 4940
F 0 "#PWR0153" H 3910 4690 50  0001 C CNN
F 1 "GND" H 3915 4767 50  0000 C CNN
F 2 "" H 3910 4940 50  0001 C CNN
F 3 "" H 3910 4940 50  0001 C CNN
	1    3910 4940
	1    0    0    -1  
$EndComp
Connection ~ 2360 3090
Text Label 1100 850  0    50   ~ 0
V_HIGH
Text HLabel 850  850  0    50   Input ~ 0
V_HIGH
Wire Wire Line
	850  850  1100 850 
Wire Notes Line
	500  650  1400 650 
Wire Wire Line
	6710 3590 6860 3590
Connection ~ 6710 3590
$Comp
L power:GND #PWR04
U 1 1 5B9ACD6B
P 8680 1580
F 0 "#PWR04" H 8680 1330 50  0001 C CNN
F 1 "GND" H 8685 1407 50  0000 C CNN
F 2 "" H 8680 1580 50  0001 C CNN
F 3 "" H 8680 1580 50  0001 C CNN
	1    8680 1580
	1    0    0    -1  
$EndComp
Text Label 10440 3280 0    50   ~ 0
5V_IN
Wire Wire Line
	8680 1530 8680 1580
Text Label 1260 2740 2    50   ~ 0
V_HIGH
Connection ~ 2360 4890
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5B9E51E9
P 6710 3540
F 0 "#FLG0101" H 6710 3615 50  0001 C CNN
F 1 "PWR_FLAG" H 6710 3714 50  0000 C CNN
F 2 "" H 6710 3540 50  0001 C CNN
F 3 "~" H 6710 3540 50  0001 C CNN
	1    6710 3540
	1    0    0    -1  
$EndComp
Wire Wire Line
	6710 3590 6710 3540
$Comp
L Wind_module_lib:0505-5V U21
U 1 1 5B9F587D
P 9600 5720
F 0 "U21" H 9878 5435 50  0000 L CNN
F 1 "ROE_1505S" H 9878 5344 50  0000 L CNN
F 2 "Wind_module:TME_0505s" H 9600 5720 50  0001 C CNN
F 3 "" H 9600 5720 50  0001 C CNN
	1    9600 5720
	1    0    0    -1  
$EndComp
Text Label 9050 5720 2    50   ~ 0
15V
$Comp
L power:GND #PWR0154
U 1 1 5B9F5887
P 9450 6320
F 0 "#PWR0154" H 9450 6070 50  0001 C CNN
F 1 "GND" H 9455 6147 50  0000 C CNN
F 2 "" H 9450 6320 50  0001 C CNN
F 3 "" H 9450 6320 50  0001 C CNN
	1    9450 6320
	1    0    0    -1  
$EndComp
Text Label 10600 5570 0    50   ~ 0
5V
$Comp
L power:GNDD #PWR0155
U 1 1 5B9F588F
P 9650 6320
F 0 "#PWR0155" H 9650 6070 50  0001 C CNN
F 1 "GNDD" H 9654 6165 50  0000 C CNN
F 2 "" H 9650 6320 50  0001 C CNN
F 3 "" H 9650 6320 50  0001 C CNN
	1    9650 6320
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 6270 9750 6370
Wire Wire Line
	9650 6270 9650 6320
$Comp
L Wind_module_lib:0505-5V U20
U 1 1 5B9F9A44
P 9600 4220
F 0 "U20" H 9878 3935 50  0000 L CNN
F 1 "ROE_1505S" H 9878 3844 50  0000 L CNN
F 2 "Wind_module:TME_0505s" H 9600 4220 50  0001 C CNN
F 3 "" H 9600 4220 50  0001 C CNN
	1    9600 4220
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 4770 9450 4820
Wire Wire Line
	9450 5520 9450 5570
$Comp
L power:GND #PWR0157
U 1 1 5BA31DDA
P 9450 4820
F 0 "#PWR0157" H 9450 4570 50  0001 C CNN
F 1 "GND" H 9455 4647 50  0000 C CNN
F 2 "" H 9450 4820 50  0001 C CNN
F 3 "" H 9450 4820 50  0001 C CNN
	1    9450 4820
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0158
U 1 1 5BA31E1D
P 9450 5570
F 0 "#PWR0158" H 9450 5320 50  0001 C CNN
F 1 "GND" H 9455 5397 50  0000 C CNN
F 2 "" H 9450 5570 50  0001 C CNN
F 3 "" H 9450 5570 50  0001 C CNN
	1    9450 5570
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0160
U 1 1 5BA31E60
P 9650 5570
F 0 "#PWR0160" H 9650 5320 50  0001 C CNN
F 1 "GNDD" H 9654 5415 50  0000 C CNN
F 2 "" H 9650 5570 50  0001 C CNN
F 3 "" H 9650 5570 50  0001 C CNN
	1    9650 5570
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0161
U 1 1 5BA31EA3
P 9650 4820
F 0 "#PWR0161" H 9650 4570 50  0001 C CNN
F 1 "GNDD" H 9654 4665 50  0000 C CNN
F 2 "" H 9650 4820 50  0001 C CNN
F 3 "" H 9650 4820 50  0001 C CNN
	1    9650 4820
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 4770 9650 4820
Wire Wire Line
	9750 4770 9750 4820
Wire Wire Line
	9750 4820 10500 4820
Wire Wire Line
	10500 4820 10500 5570
Wire Wire Line
	9750 5520 9750 5570
Wire Wire Line
	9750 5570 10500 5570
Connection ~ 10500 5570
Wire Wire Line
	9750 6370 10500 6370
Wire Wire Line
	10500 5570 10500 6370
Wire Wire Line
	10500 5570 10600 5570
Wire Wire Line
	9650 5520 9650 5570
$Comp
L power:GND #PWR0162
U 1 1 5BAA6811
P 9640 3380
F 0 "#PWR0162" H 9640 3130 50  0001 C CNN
F 1 "GND" H 9645 3207 50  0000 C CNN
F 2 "" H 9640 3380 50  0001 C CNN
F 3 "" H 9640 3380 50  0001 C CNN
	1    9640 3380
	1    0    0    -1  
$EndComp
Wire Wire Line
	9740 3130 9740 3280
$Comp
L device:LED D16
U 1 1 5BB3409D
P 1800 7180
F 0 "D16" V 1838 7063 50  0000 R CNN
F 1 "LED" V 1747 7063 50  0000 R CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1800 7180 50  0001 C CNN
F 3 "~" H 1800 7180 50  0001 C CNN
	1    1800 7180
	0    -1   -1   0   
$EndComp
$Comp
L device:R R85
U 1 1 5BB340A4
P 1800 6780
F 0 "R85" H 1870 6826 50  0000 L CNN
F 1 "220" H 1870 6735 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1730 6780 50  0001 C CNN
F 3 "~" H 1800 6780 50  0001 C CNN
	1    1800 6780
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 6930 1800 7030
Wire Wire Line
	1800 7430 1800 7330
Text Label 1800 6530 1    50   ~ 0
5V
Wire Wire Line
	1800 6630 1800 6530
$Comp
L device:LED D17
U 1 1 5BB38EE8
P 2250 7180
F 0 "D17" V 2288 7063 50  0000 R CNN
F 1 "LED" V 2197 7063 50  0000 R CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2250 7180 50  0001 C CNN
F 3 "~" H 2250 7180 50  0001 C CNN
	1    2250 7180
	0    -1   -1   0   
$EndComp
$Comp
L device:R R86
U 1 1 5BB38EEF
P 2250 6780
F 0 "R86" H 2320 6826 50  0000 L CNN
F 1 "220" H 2320 6735 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2180 6780 50  0001 C CNN
F 3 "~" H 2250 6780 50  0001 C CNN
	1    2250 6780
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6930 2250 7030
Text Label 2250 6530 1    50   ~ 0
5V_N
Wire Wire Line
	2250 6630 2250 6530
$Comp
L device:R R87
U 1 1 5B9C721D
P 4510 4490
F 0 "R87" H 4580 4536 50  0000 L CNN
F 1 "150k" H 4580 4445 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4440 4490 50  0001 C CNN
F 3 "~" H 4510 4490 50  0001 C CNN
	1    4510 4490
	1    0    0    -1  
$EndComp
$Comp
L device:R R88
U 1 1 5B9C72A5
P 4810 4490
F 0 "R88" H 4880 4536 50  0000 L CNN
F 1 "150k" H 4880 4445 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4740 4490 50  0001 C CNN
F 3 "~" H 4810 4490 50  0001 C CNN
	1    4810 4490
	1    0    0    -1  
$EndComp
$Comp
L device:R R89
U 1 1 5B9C7301
P 5110 4490
F 0 "R89" H 5180 4536 50  0000 L CNN
F 1 "150k" H 5180 4445 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5040 4490 50  0001 C CNN
F 3 "~" H 5110 4490 50  0001 C CNN
	1    5110 4490
	1    0    0    -1  
$EndComp
$Comp
L device:R R90
U 1 1 5B9C735F
P 5410 4490
F 0 "R90" H 5480 4536 50  0000 L CNN
F 1 "150k" H 5480 4445 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5340 4490 50  0001 C CNN
F 3 "~" H 5410 4490 50  0001 C CNN
	1    5410 4490
	1    0    0    -1  
$EndComp
Wire Wire Line
	3910 4890 4510 4890
$Comp
L device:R R91
U 1 1 5B9CBE47
P 5760 4490
F 0 "R91" H 5830 4536 50  0000 L CNN
F 1 "10k" H 5830 4445 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5690 4490 50  0001 C CNN
F 3 "~" H 5760 4490 50  0001 C CNN
	1    5760 4490
	1    0    0    -1  
$EndComp
Wire Wire Line
	4510 4640 4510 4890
Connection ~ 4510 4890
Wire Wire Line
	4510 4890 4810 4890
Wire Wire Line
	4810 4640 4810 4890
Connection ~ 4810 4890
Wire Wire Line
	4810 4890 5110 4890
Wire Wire Line
	5110 4640 5110 4890
Connection ~ 5110 4890
Wire Wire Line
	5110 4890 5410 4890
Wire Wire Line
	5410 4640 5410 4890
Connection ~ 5410 4890
Wire Wire Line
	5410 4890 5760 4890
Wire Wire Line
	5760 4640 5760 4890
Connection ~ 5760 4890
Wire Wire Line
	5760 4890 6110 4890
Wire Wire Line
	5760 4340 5760 4140
Wire Wire Line
	5760 4140 5610 4140
Connection ~ 5610 4140
Wire Wire Line
	5410 4340 5410 4140
Connection ~ 5410 4140
Wire Wire Line
	5410 4140 5610 4140
Wire Wire Line
	5110 4340 5110 4140
Connection ~ 5110 4140
Wire Wire Line
	5110 4140 5410 4140
Wire Wire Line
	4810 4340 4810 4140
Connection ~ 4810 4140
Wire Wire Line
	4810 4140 5110 4140
Wire Wire Line
	4510 4340 4510 4140
Connection ~ 4510 4140
Wire Wire Line
	4510 4140 4810 4140
$Comp
L Connector_Generic:Conn_01x05 J4
U 1 1 5BA2F59A
P 1660 3090
F 0 "J4" H 1740 3132 50  0000 L CNN
F 1 "Conn_Power_Supply_higher_60V" H 1310 2740 50  0000 L CNN
F 2 "Wind_module:Connector_5pins" H 1660 3090 50  0001 C CNN
F 3 "~" H 1660 3090 50  0001 C CNN
	1    1660 3090
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1860 3090 2360 3090
NoConn ~ 1860 2990
Wire Wire Line
	1960 2740 1960 2890
Wire Wire Line
	1960 2890 1860 2890
Wire Wire Line
	1260 2740 1960 2740
Wire Wire Line
	1860 3290 1910 3290
Wire Wire Line
	1860 3190 1910 3190
Wire Wire Line
	1910 3190 1910 3290
Connection ~ 1910 3290
Wire Wire Line
	1910 3290 2060 3290
Wire Wire Line
	2060 3290 2060 4890
Wire Wire Line
	2060 4890 2360 4890
$Comp
L power:GNDD #PWR013
U 1 1 5BBA2D28
P 1800 7430
F 0 "#PWR013" H 1800 7180 50  0001 C CNN
F 1 "GNDD" H 1804 7275 50  0000 C CNN
F 2 "" H 1800 7430 50  0001 C CNN
F 3 "" H 1800 7430 50  0001 C CNN
	1    1800 7430
	1    0    0    -1  
$EndComp
Text Label 2300 7480 0    50   ~ 0
N
Wire Wire Line
	2250 7480 2300 7480
Wire Wire Line
	2250 7330 2250 7480
NoConn ~ 3510 3690
Wire Notes Line
	500  950  1400 950 
Wire Notes Line
	1400 950  1400 500 
Wire Notes Line
	11200 1350 9600 1350
Wire Notes Line
	9600 1350 9600 500 
Wire Wire Line
	9550 6470 9150 6470
Wire Wire Line
	9550 6270 9550 6470
Wire Wire Line
	9550 5720 9150 5720
Wire Wire Line
	9550 5520 9550 5720
Connection ~ 9150 5720
Wire Wire Line
	9150 5720 9150 6470
Wire Wire Line
	9550 4970 9150 4970
Wire Wire Line
	9550 4770 9550 4970
Wire Wire Line
	9450 6320 9450 6270
Wire Wire Line
	9150 4970 9150 5720
Wire Wire Line
	9050 5720 9150 5720
Wire Wire Line
	9540 3580 9140 3580
Wire Wire Line
	9540 3130 9540 3580
Wire Wire Line
	9440 3130 9440 3180
Wire Wire Line
	8880 1780 8980 1780
$Comp
L Wind_module_lib:0505-5V U17
U 1 1 5BD1504D
P 8830 980
F 0 "U17" H 9108 695 50  0000 L CNN
F 1 "ROE_1505S" H 9108 604 50  0000 L CNN
F 2 "Wind_module:TME_0505s" H 8830 980 50  0001 C CNN
F 3 "" H 8830 980 50  0001 C CNN
	1    8830 980 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8380 1880 8780 1880
Wire Wire Line
	8780 1880 8780 1530
Wire Wire Line
	8880 1530 8880 1780
Wire Wire Line
	8980 1530 8980 1680
Wire Wire Line
	8980 1680 9480 1680
Wire Wire Line
	9740 3280 10440 3280
Wire Notes Line
	600  5530 3800 5530
Wire Notes Line
	3800 5530 3800 7680
Text Notes 600  5730 0    47   ~ 0
FEEDER OPERATION LEDs
Text Notes 610  6080 0    47   ~ 0
These LEDs show if the feeders are operational. \nThere is one of the GND referenced 5V, \none for the NEUTRAL referenced 5V, \nand one for the isolated 5V. 
Text Label 8380 1880 2    50   ~ 0
15V
Wire Notes Line
	7840 690  7840 2120
Wire Notes Line
	7840 2120 10620 2120
Text Notes 7950 670  0    47   ~ 0
NEUTRAL REFERENCED 5V
Text Label 9140 3580 0    50   ~ 0
15V
Wire Notes Line
	8530 2290 8530 6390
Wire Notes Line
	8530 6390 8540 6390
Wire Notes Line
	8740 3880 11040 3880
Wire Notes Line
	11040 3880 11040 3890
Text Notes 8780 2350 0    47   ~ 0
NEUTRAL REFERENCED 5V
Text Notes 8730 4030 0    47   ~ 0
ISOLATED 5V
Text Notes 9360 4250 0    47   ~ 0
These 3 ROE1505 are connected in parallel \nbecause each can only supply up to 200mA. \nThe microcontroller can consume up to \n500mA, requiring at least 3 of them. 
Text Notes 1520 650  0    47   ~ 0
FEEDER BLOCK
Text Notes 1520 1130 0    47   ~ 0
This block regroups all the feeder circuits used with this converter. \n- There is a main feeder that converts 15V down from 60V in its input.\n- There is a 5V NEUTRAL referenced feeder that supplies energy to the voltage measurement chains\n- There is a 5V GROUND referenced feeder that supplies energy to the current and temperature measurement chains. \n- There is an ISOLATED 5V that supplies energy to the slave board. 
Wire Notes Line
	1400 980  1400 1220
Wire Notes Line
	1400 1220 6020 1220
Wire Notes Line
	6020 1220 6020 560 
Wire Notes Line
	1460 700  2460 700 
Wire Notes Line
	2460 700  2460 550 
Wire Notes Line
	2210 1270 2210 3520
Wire Notes Line
	2190 3550 590  3550
Text Notes 510  1120 0    47   ~ 0
MAGIC INPUT
Text Notes 520  2240 0    47   ~ 0
V_HIGH can be as high as 600V for some\napplications. The maximum input voltage \nof the LMR16010 is 60V, which is a problem.\n\nTo avoid delaying the development of the \nboard, a connector was introduced for now.\n\nEventually it will be necessary to develop\na tiny power converter that goes from\n600V to 60V in order to suplly the board \nunder any circumstance.\n\n
Text Notes 2390 2900 0    47   ~ 0
MAIN FEEDER Design Notes\n------------------------------\nVin 17V to 60V // Vout 15.8V, 1A\n---INDUCTOR AND CURRENT---\nInductor = 12LRS333C (Murata) (or higher)\nDiL = 0.5A\n\n---FEEDBACK ELEMENTS---\nFB = 0.75V (Quite precise with the bridge divider)\nRT = 30k (3x10k) (for an operation around 700kHz)\n\n--CAPACITORS VOLTAGE----\nC39 = 20u (should hold between 100V and 600V, thin-film) \nC40 = 4.7u (should hold between 100V and 600V, ceramic)\nC42 = 20u (150% bigger than the minimum, should hold up to 35V)\nCb = 100n (ceramic, X7R, for good thermal stability)\n\n--SCHOTTKY DIODE--\nMBR2H200SFT3G - 200V, 2A, SOD123\nS1GHE - 400V, 1A, SOD123
Text Notes 4370 5040 0    47   ~ 0
FEEDBACK BRIDGE DIVIDER
Text Notes 4390 6230 0    47   ~ 0
The LMR16010 supplies 15V with a feedback voltage of 0.75V\nin its fb pin. This bridge divider was designed to provide these\n0.75V using only 150k and 10k resistors. \n\nVout = 1 =   0.75\n Vin   21   15+0.75\n\nUsing 150k on top gives a resistor of 7.5k on the bottom\n\nUsing the 5 resistances in parallel gives:\n\n150k divided by 4 = 37.5k\n37.5k in parallel with 10k = 7.89k\nWhich should give 0.749V. \n
Text Notes 4420 5550 0    47   ~ 0
___    ___   ______
Wire Notes Line
	4320 5070 6670 5070
Wire Notes Line
	4270 4220 5520 4220
Wire Notes Line
	5520 4220 5520 3640
Wire Notes Line
	5520 3640 5980 3640
Wire Notes Line
	5980 3640 5980 4980
Wire Notes Line
	5980 4980 6710 4980
Wire Notes Line
	6710 4980 6710 6380
Wire Notes Line
	6710 6380 4270 6380
Wire Notes Line
	4270 4220 4270 6380
$EndSCHEMATC
