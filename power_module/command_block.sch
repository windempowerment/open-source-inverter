EESchema Schematic File Version 4
LIBS:Wind_module-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x18_Top_Bottom J3
U 1 1 5B8E5569
P 5260 3090
F 0 "J3" H 5310 4107 50  0000 C CNN
F 1 "PCI36pos" H 5310 4016 50  0000 C CNN
F 2 "Wind_module:PCI36POS_CON_1" H 5260 3090 50  0001 C CNN
F 3 "~" H 5260 3090 50  0001 C CNN
	1    5260 3090
	1    0    0    -1  
$EndComp
Text Label 4660 2290 2    50   ~ 0
5V
Text Label 4660 2590 2    50   ~ 0
VCC_3V3
Text Label 3000 2790 2    50   ~ 0
MISO
Text Label 3000 2890 2    50   ~ 0
MOSI
Text Label 3000 2990 2    50   ~ 0
SS1
Text Label 3000 3090 2    50   ~ 0
RA2
Text Label 3000 3190 2    50   ~ 0
CLK
Text Label 3000 3290 2    50   ~ 0
SS
$Comp
L power:GNDD #PWR0133
U 1 1 5B8E568F
P 4260 3440
F 0 "#PWR0133" H 4260 3190 50  0001 C CNN
F 1 "GNDD" H 4264 3285 50  0000 C CNN
F 2 "" H 4260 3440 50  0001 C CNN
F 3 "" H 4260 3440 50  0001 C CNN
	1    4260 3440
	1    0    0    -1  
$EndComp
Wire Wire Line
	4660 2290 5010 2290
Wire Wire Line
	5060 2390 5010 2390
Wire Wire Line
	5060 2590 5010 2590
Wire Wire Line
	5060 2790 3000 2790
Wire Wire Line
	5060 2890 3000 2890
Wire Wire Line
	3000 2990 5060 2990
Wire Wire Line
	5060 3090 3000 3090
Wire Wire Line
	3000 3190 5060 3190
Wire Wire Line
	3000 3290 5060 3290
Text Label 3620 3970 2    50   ~ 0
PWM3H
Text Label 3620 4070 2    50   ~ 0
PWM3L
Text Label 3620 4170 2    50   ~ 0
PWM2H
Text Label 3620 4270 2    50   ~ 0
PWM2L
Text Label 3620 4370 2    50   ~ 0
PWM1H
Text Label 3620 4470 2    50   ~ 0
PWM1L
Wire Wire Line
	5560 2290 6060 2290
Wire Wire Line
	6060 2290 6060 2330
Wire Wire Line
	6060 2390 5560 2390
Wire Wire Line
	5560 2490 6060 2490
Wire Wire Line
	6060 2490 6060 2390
Connection ~ 6060 2390
Wire Wire Line
	6060 2490 6060 2590
Wire Wire Line
	6060 2590 5560 2590
Connection ~ 6060 2490
Wire Wire Line
	5560 2690 6060 2690
Wire Wire Line
	6060 2690 6060 2590
Connection ~ 6060 2590
$Comp
L power:GNDD #PWR0134
U 1 1 5B8E7435
P 6320 2510
F 0 "#PWR0134" H 6320 2260 50  0001 C CNN
F 1 "GNDD" H 6324 2355 50  0000 C CNN
F 2 "" H 6320 2510 50  0001 C CNN
F 3 "" H 6320 2510 50  0001 C CNN
	1    6320 2510
	1    0    0    -1  
$EndComp
Text Label 7290 2790 0    50   ~ 0
AN5
Text Label 7290 2890 0    50   ~ 0
AN4
Text Label 7290 2990 0    50   ~ 0
AN3
Text Label 7290 3090 0    50   ~ 0
AN2
Text Label 7290 3190 0    50   ~ 0
AN1
Text Label 7290 3290 0    50   ~ 0
AN0
Wire Wire Line
	7290 2790 5560 2790
Wire Wire Line
	5560 2890 7290 2890
Wire Wire Line
	7290 2990 5560 2990
Wire Wire Line
	5560 3090 7290 3090
Wire Wire Line
	7290 3190 5560 3190
Wire Wire Line
	5560 3290 7290 3290
Wire Wire Line
	6160 3390 6160 3490
Connection ~ 6160 3690
Connection ~ 6160 3590
Wire Wire Line
	6160 3590 6160 3690
Connection ~ 6160 3490
Wire Wire Line
	6160 3490 6160 3590
$Comp
L power:GNDD #PWR0135
U 1 1 5B8EAB3F
P 6160 4040
F 0 "#PWR0135" H 6160 3790 50  0001 C CNN
F 1 "GNDD" H 6164 3885 50  0000 C CNN
F 2 "" H 6160 4040 50  0001 C CNN
F 3 "" H 6160 4040 50  0001 C CNN
	1    6160 4040
	1    0    0    -1  
$EndComp
Wire Wire Line
	4260 3440 4260 3390
Wire Wire Line
	4260 3390 5060 3390
Text HLabel 10750 900  2    50   Output ~ 0
PWM1L
Text HLabel 10750 1000 2    50   Output ~ 0
PWM1H
Text HLabel 10750 1100 2    50   Output ~ 0
PWM2L
$Comp
L Connector:TestPoint TP5
U 1 1 5B8ED6AD
P 1900 6140
F 0 "TP5" H 1958 6260 50  0000 L CNN
F 1 "AN2" H 1958 6169 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 2100 6140 50  0001 C CNN
F 3 "~" H 2100 6140 50  0001 C CNN
	1    1900 6140
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 5B8ED8D1
P 2450 6140
F 0 "TP6" H 2508 6260 50  0000 L CNN
F 1 "AN3" H 2508 6169 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 2650 6140 50  0001 C CNN
F 3 "~" H 2650 6140 50  0001 C CNN
	1    2450 6140
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5B8ED924
P 3000 6140
F 0 "TP7" H 3058 6260 50  0000 L CNN
F 1 "AN4" H 3058 6169 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 3200 6140 50  0001 C CNN
F 3 "~" H 3200 6140 50  0001 C CNN
	1    3000 6140
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5B8ED954
P 3550 6140
F 0 "TP8" H 3608 6260 50  0000 L CNN
F 1 "AN5" H 3608 6169 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 3750 6140 50  0001 C CNN
F 3 "~" H 3750 6140 50  0001 C CNN
	1    3550 6140
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5B8ED984
P 1400 6140
F 0 "TP4" H 1458 6260 50  0000 L CNN
F 1 "AN1" H 1458 6169 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 1600 6140 50  0001 C CNN
F 3 "~" H 1600 6140 50  0001 C CNN
	1    1400 6140
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5B8ED9B0
P 900 6140
F 0 "TP3" H 958 6260 50  0000 L CNN
F 1 "AN0" H 958 6169 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 1100 6140 50  0001 C CNN
F 3 "~" H 1100 6140 50  0001 C CNN
	1    900  6140
	1    0    0    -1  
$EndComp
Text Label 900  6290 2    50   ~ 0
AN0
Text Label 1400 6290 2    50   ~ 0
AN1
Text Label 1900 6290 2    50   ~ 0
AN2
Text Label 2450 6290 2    50   ~ 0
AN3
Text Label 3000 6290 2    50   ~ 0
AN4
Text Label 3550 6290 2    50   ~ 0
AN5
Wire Wire Line
	900  6290 900  6140
Wire Wire Line
	1400 6140 1400 6290
Wire Wire Line
	1900 6140 1900 6290
Wire Wire Line
	2450 6140 2450 6290
Wire Wire Line
	3000 6140 3000 6290
Wire Wire Line
	3550 6140 3550 6290
Text HLabel 10750 1200 2    50   Output ~ 0
PWM2H
Text HLabel 10750 1300 2    50   Output ~ 0
PWM3L
Text HLabel 10750 1400 2    50   Output ~ 0
PWM3H
Text Label 10500 1400 2    50   ~ 0
PWM3H
Text Label 10500 1300 2    50   ~ 0
PWM3L
Text Label 10500 1200 2    50   ~ 0
PWM2H
Text Label 10500 1100 2    50   ~ 0
PWM2L
Text Label 10500 1000 2    50   ~ 0
PWM1H
Text Label 10500 900  2    50   ~ 0
PWM1L
Wire Wire Line
	10500 900  10750 900 
Wire Wire Line
	10750 1000 10500 1000
Wire Wire Line
	10500 1100 10750 1100
Wire Wire Line
	10750 1200 10500 1200
Wire Wire Line
	10500 1300 10750 1300
Wire Wire Line
	10750 1400 10500 1400
Text HLabel 900  1000 0    50   Input ~ 0
5V
Text Label 1350 1000 0    50   ~ 0
5V
Text Label 1350 900  0    50   ~ 0
MISO
Text Label 10500 1500 2    50   ~ 0
MOSI
Text Label 10500 1600 2    50   ~ 0
SS
Text Label 10500 1800 2    50   ~ 0
CLK
Text Label 10500 1700 2    50   ~ 0
SS1
$Comp
L Connector:TestPoint TP2
U 1 1 5B8FB8AA
P 6340 7460
F 0 "TP2" H 6398 7580 50  0000 L CNN
F 1 "RA2" H 6398 7489 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 6540 7460 50  0001 C CNN
F 3 "~" H 6540 7460 50  0001 C CNN
	1    6340 7460
	1    0    0    -1  
$EndComp
Text Label 6340 7610 2    50   ~ 0
RA2
Wire Wire Line
	6340 7460 6340 7610
Text HLabel 900  900  0    50   Input ~ 0
MISO
Wire Wire Line
	1350 900  900  900 
Wire Wire Line
	900  1000 1350 1000
Text HLabel 10750 1500 2    50   Output ~ 0
MOSI
Text HLabel 10750 1600 2    50   Output ~ 0
SS
Text HLabel 10750 1700 2    50   Output ~ 0
SS1
Text HLabel 10750 1800 2    50   Output ~ 0
CLK
Wire Wire Line
	10500 1500 10750 1500
Wire Wire Line
	10500 1600 10750 1600
Wire Wire Line
	10500 1700 10750 1700
Wire Wire Line
	10500 1800 10750 1800
Wire Wire Line
	5010 2490 5010 2390
Wire Wire Line
	5010 2490 5060 2490
Connection ~ 5010 2390
Wire Wire Line
	5010 2290 5010 2390
Connection ~ 5010 2290
Wire Wire Line
	5010 2290 5060 2290
Wire Wire Line
	5010 2690 5010 2590
Wire Wire Line
	5010 2690 5060 2690
Connection ~ 5010 2590
Wire Wire Line
	5010 2590 4810 2590
Wire Notes Line
	500  800  1800 800 
Wire Notes Line
	10050 800  11200 800 
Text Notes 10350 650  0    50   ~ 0
BLOCK OUTPUTS
Text Notes 800  650  0    50   ~ 0
BLOCK INTPUTS
Wire Notes Line
	500  1100 1800 1100
Wire Notes Line
	1800 1100 1800 500 
Wire Wire Line
	5560 3390 6160 3390
Wire Wire Line
	5560 3490 6160 3490
Wire Wire Line
	5560 3590 6160 3590
Wire Wire Line
	5560 3690 6160 3690
Wire Wire Line
	6160 3990 6160 4040
Wire Wire Line
	5560 3990 6160 3990
Wire Wire Line
	6160 3690 6160 3790
Connection ~ 6160 3990
Wire Wire Line
	5560 3790 6160 3790
Connection ~ 6160 3790
Wire Wire Line
	6160 3790 6160 3890
Wire Wire Line
	5560 3890 6160 3890
Connection ~ 6160 3890
Wire Wire Line
	6160 3890 6160 3990
$Comp
L Connector:TestPoint TP1
U 1 1 5BA413AF
P 910 7400
F 0 "TP1" H 968 7520 50  0000 L CNN
F 1 "PWM1L" H 968 7429 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 1110 7400 50  0001 C CNN
F 3 "~" H 1110 7400 50  0001 C CNN
	1    910  7400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5BA41429
P 1410 7400
F 0 "TP10" H 1468 7520 50  0000 L CNN
F 1 "PWM1H" H 1468 7429 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 1610 7400 50  0001 C CNN
F 3 "~" H 1610 7400 50  0001 C CNN
	1    1410 7400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP12
U 1 1 5BA4145D
P 1910 7400
F 0 "TP12" H 1968 7520 50  0000 L CNN
F 1 "PWM2L" H 1968 7429 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 2110 7400 50  0001 C CNN
F 3 "~" H 2110 7400 50  0001 C CNN
	1    1910 7400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP13
U 1 1 5BA41493
P 2460 7400
F 0 "TP13" H 2518 7520 50  0000 L CNN
F 1 "PWM2h" H 2518 7429 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 2660 7400 50  0001 C CNN
F 3 "~" H 2660 7400 50  0001 C CNN
	1    2460 7400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP14
U 1 1 5BA42D32
P 3010 7400
F 0 "TP14" H 3068 7520 50  0000 L CNN
F 1 "PWM3L" H 3068 7429 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 3210 7400 50  0001 C CNN
F 3 "~" H 3210 7400 50  0001 C CNN
	1    3010 7400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP15
U 1 1 5BA42D6E
P 3560 7400
F 0 "TP15" H 3618 7520 50  0000 L CNN
F 1 "PWM3H" H 3618 7429 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 3760 7400 50  0001 C CNN
F 3 "~" H 3760 7400 50  0001 C CNN
	1    3560 7400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP9
U 1 1 5BA47716
P 4460 6180
F 0 "TP9" H 4518 6300 50  0000 L CNN
F 1 "SS" H 4518 6209 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 4660 6180 50  0001 C CNN
F 3 "~" H 4660 6180 50  0001 C CNN
	1    4460 6180
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5BA47758
P 4960 6180
F 0 "TP11" H 5018 6300 50  0000 L CNN
F 1 "SS1" H 5018 6209 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 5160 6180 50  0001 C CNN
F 3 "~" H 5160 6180 50  0001 C CNN
	1    4960 6180
	1    0    0    -1  
$EndComp
Text Label 4460 6330 2    50   ~ 0
SS
Text Label 4960 6330 2    50   ~ 0
SS1
Text Label 910  7550 2    50   ~ 0
PWM1L
Text Label 1410 7550 2    50   ~ 0
PWM1H
Text Label 1910 7550 2    50   ~ 0
PWM2L
Text Label 2460 7550 2    50   ~ 0
PWM2H
Text Label 3010 7550 2    50   ~ 0
PWM3L
Text Label 3560 7550 2    50   ~ 0
PWM3H
Wire Wire Line
	3560 7400 3560 7550
Wire Wire Line
	3010 7400 3010 7550
Wire Wire Line
	2460 7550 2460 7400
Wire Wire Line
	1910 7400 1910 7550
Wire Wire Line
	1410 7550 1410 7400
Wire Wire Line
	910  7400 910  7550
Wire Wire Line
	4460 6180 4460 6330
Wire Wire Line
	4960 6180 4960 6330
Text Label 10500 1900 2    50   ~ 0
VCC_3V3
Wire Notes Line
	10050 1950 11200 1950
Wire Notes Line
	10050 500  10050 1950
Text HLabel 10750 1900 2    50   Output ~ 0
VCC_3V3
Wire Wire Line
	10500 1900 10750 1900
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5BFBD1AF
P 4810 2540
F 0 "#FLG0105" H 4810 2615 50  0001 C CNN
F 1 "PWR_FLAG" H 4810 2714 50  0000 C CNN
F 2 "" H 4810 2540 50  0001 C CNN
F 3 "~" H 4810 2540 50  0001 C CNN
	1    4810 2540
	1    0    0    -1  
$EndComp
Connection ~ 4810 2590
Wire Wire Line
	4810 2590 4660 2590
Wire Wire Line
	4810 2590 4810 2540
$Comp
L Connector:TestPoint TP16
U 1 1 5BEA6952
P 4720 7330
F 0 "TP16" H 4778 7450 50  0000 L CNN
F 1 "GNDD" H 4778 7359 50  0000 L CNN
F 2 "Wind_module:TestPoint_Loop_D1.80mm_Drill1.0mm_Beaded" H 4920 7330 50  0001 C CNN
F 3 "~" H 4920 7330 50  0001 C CNN
	1    4720 7330
	1    0    0    -1  
$EndComp
Wire Wire Line
	4720 7330 4720 7480
$Comp
L power:GNDD #PWR0117
U 1 1 5BEA8BBE
P 4720 7480
F 0 "#PWR0117" H 4720 7230 50  0001 C CNN
F 1 "GNDD" H 4724 7325 50  0000 C CNN
F 2 "" H 4720 7480 50  0001 C CNN
F 3 "" H 4720 7480 50  0001 C CNN
	1    4720 7480
	1    0    0    -1  
$EndComp
Wire Notes Line
	6880 6530 500  6530
Wire Notes Line
	500  6530 500  6540
Wire Notes Line
	4200 6620 4200 7670
Wire Notes Line
	570  5340 7000 5340
Wire Notes Line
	7000 5340 7000 6450
Wire Notes Line
	7000 6450 6990 6450
Wire Notes Line
	4200 5490 4200 6360
Wire Notes Line
	4200 6360 4210 6360
Wire Wire Line
	4430 3970 4430 3490
Wire Wire Line
	4430 3490 5060 3490
Wire Wire Line
	3620 3970 4430 3970
Wire Wire Line
	4510 4070 4510 3590
Wire Wire Line
	4510 3590 5060 3590
Wire Wire Line
	3620 4070 4510 4070
Wire Wire Line
	4580 4170 4580 3690
Wire Wire Line
	4580 3690 5060 3690
Wire Wire Line
	3620 4170 4580 4170
Wire Wire Line
	4660 4270 4660 3790
Wire Wire Line
	4660 3790 5060 3790
Wire Wire Line
	3620 4270 4660 4270
Wire Wire Line
	4730 4370 4730 3890
Wire Wire Line
	4730 3890 5060 3890
Wire Wire Line
	3620 4370 4730 4370
Wire Wire Line
	4800 4470 4800 3990
Wire Wire Line
	4800 3990 5060 3990
Wire Wire Line
	3620 4470 4800 4470
Wire Wire Line
	6320 2510 6320 2330
Wire Wire Line
	6320 2330 6060 2330
Connection ~ 6060 2330
Wire Wire Line
	6060 2330 6060 2390
Wire Notes Line
	4100 3720 4100 4810
Wire Notes Line
	4100 4810 2580 4810
Wire Notes Line
	2580 4810 2580 3720
Wire Notes Line
	2580 3720 4100 3720
Wire Notes Line
	2490 3530 4140 3530
Wire Notes Line
	4140 3530 4140 2320
Wire Notes Line
	4140 2320 2490 2320
Wire Notes Line
	2490 2320 2490 3530
Wire Notes Line
	4310 2730 5060 2730
Wire Notes Line
	5060 2730 5060 1450
Wire Notes Line
	7020 3560 9410 3560
Wire Notes Line
	9410 3560 9410 2240
Wire Notes Line
	9410 2240 7020 2240
Wire Notes Line
	7020 2240 7020 3560
Text Notes 2540 2420 0    47   ~ 0
SPI CONNECTION POINTS
Text Notes 2540 2660 0    47   ~ 0
These pins allow the slave board\nto communicate with the external\nADCs via SPI.
Text Notes 2630 3810 0    47   ~ 0
PWM CONNECTION POINTS
Text Notes 2600 4760 0    47   ~ 0
These connectors allow the slave board\nto send the PWM signals to the driver\nblock.
Text Notes 7120 2360 0    47   ~ 0
ANALOG CONNECTION POINTS
Text Notes 7770 2970 0    47   ~ 0
Some extra analog I/O ports were made \navailable in case the user needs to \nacquire some other data or need \nif from some other purpose.
Text Notes 560  6680 0    47   ~ 0
PWM TEST POINTS
Text Notes 570  6940 0    47   ~ 0
These test points allow the debugging of the PWM signals \narriving from the slave board. They also allow the user to send\nsignals from some other device into the power board.
Text Notes 520  5500 0    47   ~ 0
ANALOG TEST POINTS
Text Notes 530  5760 0    47   ~ 0
These test points allow the user to connect with other devices \nsuch as sensors, through I/O pins of the slave board. \n
Text Notes 4340 5510 0    47   ~ 0
SPI TEST POINTS
Text Notes 4350 5770 0    47   ~ 0
These test points allow the user to debug which SPI is being handled\nthrough the SS and SS1 pins. 
Text Notes 4290 6710 0    47   ~ 0
DIGITAL GROUND TEST PIN
Text Notes 4290 7040 0    47   ~ 0
This pin should be placed in a \nconvenient spot for easily connecting \noscilloscope probes for debugging. 
Wire Notes Line
	5860 6690 5860 7710
Text Notes 5950 6710 0    47   ~ 0
LED TEST PIN
Text Notes 5960 7150 0    47   ~ 0
The slave board has an \nonboard LED for\nall purpose debugging. \nThis pin allows to test it \nand its output port.\n
Wire Notes Line
	4310 2720 4310 2160
Wire Notes Line
	4310 2160 3220 2160
Wire Notes Line
	3220 2160 3220 1450
Wire Notes Line
	3220 1450 5060 1450
Text Notes 3300 1560 0    47   ~ 0
POWER CONNECTION POINTS
Text Notes 3320 2010 0    47   ~ 0
These pins feed the slave board with +5V \nfrom the power board onboard feeder. \nThe VCC_3V3 pin sends the 3.3V created by \nthe slave board back into the power board \nto power the isolators.
Text Notes 1980 610  0    47   ~ 0
36-PIN CONNECTOR
Text Notes 1950 820  0    47   ~ 0
This connector allows the connection between the power and the \nslave boards. Its pinout and the purpose of each pin is described below.
$EndSCHEMATC
