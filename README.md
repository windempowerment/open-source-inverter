# Open source active rectifier and inverter for rural applications.

WindEmpowerment's open source inverter is a bidirectional inverter that permits both active rectification and inversion. 

The power limits depends on the output inductors rating that for now supports up to 7A.
The converter is meant to operate at 100kHz switching frequency.


![](Images/power_board_view.jpeg)


 The converter encompass the power power module shown above, that contains the active switchs and parrallel diodes. The power module also contains sensors that permits to get a full view on the converter operating parameters. These measures are electrically isolated from the microcontroller through a digital isolation on a common SPI bus. 


![](Images/slave_board_view.jpeg)

The converter is controlled by a microcontroller board that fits in the PCI receptable on the power module. 

Software is developped with MPLabX which can be downloaded for free.
The brain of the converter, the dsPIC33EP256 is a 16bit dsp that support MATLAB® code generation, permitting quick draft for the control laws.


# Repository architecture 


The repository has the following organization 

* Documentation.pdf
   * Contains in depth information on the chips used on the power module and microcontroller board.
* Microcontroller_board	Contains the KiCAD project of the microcontroller board 
* Power_module	Contains the KiCAD project of the power module 
* LICENSE 	CERN-OHL-S V2 open hardware license
* README 	The README you are currently reading.

# Documentation 

All files are available in this centralized repository. 
Documentation.pdf contains in depth explanation of the hardware.

# License 

This project is propelled by OwnTech Team under CERN-OHL-S open hardware Licence
The documentation provided is placed under Creative Commons SA-BY

# Disclaimer 

DISCLAIMER : This power converter is currently in alpha version and WindEmpowerment association 
does not provide garranty of any kind. 
If you attempt to replicate this converter, do it at your own risk and
USE APPROPRIATE SAFETY MEASURES AND PPE.  

# Contributors 

Luiz Villa 

Guillermo Catuogno

Jean Alinei

Paul Aalberse

Anas El Boubkari


